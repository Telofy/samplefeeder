from sqlalchemy.sql import or_, and_
from resyndicator.models import Entry
from resyndicator.fetchers.feed import FeedFetcher
from resyndicator.fetchers.content import ContentFetcher
from resyndicator.resyndicators import Resyndicator
from . import settings


RESYNDICATORS = [
    Resyndicator(
        title='Effective Altruism',
        past=settings.PAST,
        query=or_(
            Entry.source_link.in_([
                # Feeds we want to have resyndicated in full
                'http://www.animalcharityevaluators.org/feed/atom/',
                'http://feeds.feedburner.com/TheGivewellBlog',
            ]),
            or_(
                # Entries we want to select based on keywords
                Entry.title.ilike('%impact%'),
                Entry.title.ilike('%priorit%'),),
            and_(
                # From one feed, we want to include any articles with one
                # common word in the title or summary
                Entry.source_link.in_([
                    'http://80000hours.org/blog/feed.atom',
                ]), or_(
                    Entry.title.ilike('%give%'),
                    Entry.summary.ilike('%give%'),)),
    )),
]

FETCHERS = [
    FeedFetcher(
        'http://feeds.feedburner.com/TheGivewellBlog',
        interval=10*60),
    FeedFetcher(
        'http://www.goodventures.org/feeds/all',
        interval=60*60),
    FeedFetcher(
        'http://feeds.feedburner.com/gwwc-blog',
        interval=10*60),
    FeedFetcher(
        'http://feeds.feedburner.com/TheLifeYouCanSave',
        interval=60*60),
    FeedFetcher(
        'http://www.animalcharityevaluators.org/feed/atom/',
        interval=10*60),
    FeedFetcher(
        'http://globalprioritiesproject.org/feed/atom/',
        interval=10*60),
    FeedFetcher(
        'http://www.charityscience.com/1/feed',
        interval=60*60),
    FeedFetcher(
        'http://www.charityscience.com/2/feed',
        interval=60*60),
    FeedFetcher(
        'https://www.againstmalaria.com/News.ashx',
        interval=60*60),
]

CONTENT_FETCHERS = [
    ContentFetcher(past=settings.PAST),
]

STREAMS = []
