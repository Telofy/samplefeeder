from datetime import timedelta


FETCHER_SLEEP = 60
DEFAULT_LENGTH = 50
TWITTER_TITLE_LENGTH = 280
DATABASE = 'postgresql://samplefeeder:samplefeeder@localhost/samplefeeder'
BASE_URL = 'http://feeds.example.com/'
PAST = timedelta(days=30)
HUB = None
