SHELL = /bin/bash
PYTHON = $(shell which python3.7 || which python3.6 || which python3.5 || which python3.4)

default: install

bin/pip:
	${PYTHON} -m venv .

install: bin/pip
	bin/pip install pip==9.0.1 wheel==0.29.0 six==1.10.0 setuptools==35.0.2
	bin/pip install --upgrade -r requirements.txt

clean:
	rm -Rf bin include lib local
