Samplefeeder
============

Samplefeeder is a sample implementation of the Resyndicator showcasing its most important features.

Please see http://resyndicator.readthedocs.io/ for more information.
